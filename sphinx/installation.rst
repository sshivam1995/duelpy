############
Installation
############

The package can be installed via pip.

.. code-block:: Bash

    $ pip install duelpy

You can also install the project directly from the git repository. You can
install any duelpy version with this method, including the latest development
version. Be aware that this version may contain unreleased changes.

.. code-block:: Bash

    $ git clone https://gitlab.com/duelpy/duelpy.git
    $ cd duelpy
    $ pip install -e duelpy